module.exports = {
	title: "JavaScript-The-Definitive-Guide-7th-zh",
	description: "JavaScript-The-Definitive-Guide-7th-zh",
  plugins: [
    '@vuepress/active-header-links',
    '@vuepress/back-to-top',
    '@vuepress/nprogress',
    '@vuepress/pwa',
    '@vuepress/search',
  ],
};
